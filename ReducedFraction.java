


public class ReducedFraction {
    
    private int numerator;
    private int denumerator;
    
    public int numerator() {
        return numerator;
    }

    public int denumerator() {
        return denumerator;
    }

    public ReducedFraction add(ReducedFraction second){
       
       ReducedFraction sumResult= new ReducedFraction((this.numerator*second.denumerator)+(second.numerator*this.denumerator), this.denumerator*second.denumerator);

        return sumResult;
    }

    public  ReducedFraction sub (ReducedFraction second) {
        
         ReducedFraction subResult = new ReducedFraction((this.numerator*second.denumerator)-(second.numerator*this.denumerator), this.denumerator*second.denumerator);

       return subResult;
    }

}
